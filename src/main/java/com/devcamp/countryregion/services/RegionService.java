package com.devcamp.countryregion.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregion.models.Region;

@Service
public class RegionService {
    private Region hanoi = new Region("HN", "Hà Nội");
    private Region tphcm = new Region("HCM", "Thành phố Hồ Chí Minh");
    private Region danang = new Region("DN", "Đà Nẵng");

    private Region backinh = new Region("BK", "Bắc Kinh");
    private Region thuonghai = new Region("TH", "Thượng Hải");
    private Region quangchau = new Region("QC", "Quảng Châu");

    private Region newyork = new Region("NY", "New York");
    private Region washington = new Region("WS", "Washington DC");
    private Region srilanka = new Region("SK", "Srilanka");

    public ArrayList<Region> getRegionsOfVietNam() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(hanoi);
        regions.add(tphcm);
        regions.add(danang);

        return regions;
    }

    public ArrayList<Region> getRegionsOfChina() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(backinh);
        regions.add(thuonghai);
        regions.add(quangchau);

        return regions;
    }

    public ArrayList<Region> getRegionsOfUSA() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(newyork);
        regions.add(washington);
        regions.add(srilanka);

        return regions;
    }

    public Region findRegion(String regionCode) {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(hanoi);
        regions.add(tphcm);
        regions.add(danang);
        regions.add(backinh);
        regions.add(thuonghai);
        regions.add(quangchau);
        regions.add(newyork);
        regions.add(washington);
        regions.add(srilanka);

        for (Region region : regions) {
            if(region.getRegionCode().equals(regionCode)) {
                return region;
            }
        }

        return null;
    }
}
