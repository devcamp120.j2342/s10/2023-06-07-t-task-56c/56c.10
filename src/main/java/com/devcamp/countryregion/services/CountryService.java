package com.devcamp.countryregion.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregion.models.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionService;

    private Country vietNam = new Country("VN", "Việt Nam");
    private Country trungQuoc = new Country("TQ", "Trung Quốc");
    private Country usa = new Country("USA", "Mỹ");

    public ArrayList<Country> getAllCountry() {
        ArrayList<Country> countries = new ArrayList<>();

        vietNam.setRegions(regionService.getRegionsOfVietNam());
        trungQuoc.setRegions(regionService.getRegionsOfChina());
        usa.setRegions(regionService.getRegionsOfUSA());

        countries.add(vietNam);
        countries.add(trungQuoc);
        countries.add(usa);

        return countries;
    }

    public Country findCountry(String countryCode) {
        ArrayList<Country> countries = new ArrayList<>();

        vietNam.setRegions(regionService.getRegionsOfVietNam());
        trungQuoc.setRegions(regionService.getRegionsOfChina());
        usa.setRegions(regionService.getRegionsOfUSA());

        countries.add(vietNam);
        countries.add(trungQuoc);
        countries.add(usa);

        for (Country country : countries) {
            if(country.getCountryCode().equals(countryCode)) {
                return country;
            }
        }

        return null;
    }
}
