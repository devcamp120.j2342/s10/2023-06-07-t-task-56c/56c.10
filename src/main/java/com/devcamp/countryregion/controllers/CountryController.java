package com.devcamp.countryregion.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregion.models.Country;
import com.devcamp.countryregion.services.CountryService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public ArrayList<Country> getAllCountry() {
        return countryService.getAllCountry();
    }

    @GetMapping("/find-country")
    public Country findCountry(@RequestParam String countryCode) {
        return countryService.findCountry(countryCode);
    }
}
